import json
from flask import Flask, request, abort
from linebot import (LineBotApi, WebhookHandler)
from linebot.exceptions import (InvalidSignatureError)
from linebot.models import (MessageEvent, TextMessage, TextSendMessage,)


app = Flask(__name__)

# 讀取key 並載入
key_file_raw = json.loads(open('data/key.json', 'r', encoding='utf8').read())
Bot_key = key_file_raw.get('Bot')
Bot_channelSecret = Bot_key.get('channelSecret')
Bot_channelAccessToken = Bot_key.get('channelAccessToken')

line_bot_api = LineBotApi(Bot_channelAccessToken)
handler = WebhookHandler(Bot_channelSecret)
# 讀取key 並載入


@app.route("/", methods=['POST'])
def callback():
    # get X-Line-Signature header value
    signature = request.headers['X-Line-Signature']

    # get request body as text
    body = request.get_data(as_text=True)
    app.logger.info("Request body: " + body)

    # handle webhook body
    try:
        handler.handle(body, signature)
    except InvalidSignatureError:
        abort(400)
    return 'OK'


@handler.add(MessageEvent, message=TextMessage)
def handle_message(event):
    print(event)
    line_bot_api.reply_message(event.reply_token,
                               TextSendMessage(text=event.message.text))


if __name__ == "__main__":
    app.run()
